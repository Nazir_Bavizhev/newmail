package main

import (
	"encoding/json"
	"io/ioutil"
)

func parseConfigFileByPath(path string) (*Settings, error) {
	serverSettings := &Settings{}
	// reading data from JSON File
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	// Unmarshal JSON data
	err = json.Unmarshal(data, &serverSettings.Settings)
	if err != nil {
		return nil, err
	}
	return serverSettings, nil
}
