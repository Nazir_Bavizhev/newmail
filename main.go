package main

import (
	"flag"
	"fmt"
	"time"

	"go.uber.org/zap/zapcore"

	"go.uber.org/zap"
)

const timeFormat = "20060102T150405.999999999"

func main() {
	var err error
	startTime := flag.String("startTime", "past", "search start time, format=20060102T150405.999999999 in UTC")
	endTime := flag.String("endTime", "future", "search end time, format=20060102T150405.999999999 in UTC")
	configPath := flag.String("configPath", "", "absolute path to config")
	reportPath := flag.String("reportPath", "", "directory to save result")
	flag.Parse()

	if *configPath == "" {
		panic("path to config file must be defined")
	}
	if *reportPath == "" {
		*reportPath = "report.csv"
	}
	err = checkTime(*startTime)
	checkErr(err, "startTime bad format error")
	err = checkTime(*endTime)
	checkErr(err, "endTime bad format error")
	settings, err := parseConfigFileByPath(*configPath)
	checkErr(err, "error during parse config file")

	searcher, err := newSearcher(*startTime, *endTime, NewLogger())
	checkErr(err, "fail create searcher")
	searcher.logger.Info("App started with params",
		zap.String("start time", searcher.startTime),
		zap.String("end time", searcher.endTime),
	)
	searcher.Search(settings, *reportPath)

}

func NewLogger() *zap.Logger {
	cfg := zap.Config{
		Encoding:    "json",
		Level:       zap.NewAtomicLevelAt(zapcore.DebugLevel),
		OutputPaths: []string{"stdout"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:   "message",
			TimeKey:      "time",
			EncodeTime:   zapcore.ISO8601TimeEncoder,
			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}
	logger, _ := cfg.Build()
	defer logger.Sync()
	return logger

}

func checkTime(unixTime string) error {
	if unixTime == "past" || unixTime == "future" {
		return nil
	}
	_, err := time.Parse(timeFormat, unixTime)
	return err
}

func checkErr(err error, msg string) {
	if err != nil {
		panic(fmt.Sprintf("%s: %s", msg, err.Error()))
	}
}
