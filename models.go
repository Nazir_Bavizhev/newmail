package main

type (

	// Settings models
	Settings struct {
		Settings []ServerSearchSetting
	}

	ServerSearchSetting struct {
		ServerName    string              `json:"serverName"`
		WebServerAddr string              `json:"webServerAddr"`
		Login         string              `json:"login"`
		Password      string              `json:"password"`
		Cameras       []CamSearchSettings `json:"cameras"`
	}

	CamSearchSettings struct {
		ID       int     `json:"id"`
		Name     string  `json:"name"`
		Duration int     `json:"duration"`
		Shape    []Point `json:"shape"`
	}

	Point []float64

	// Search task models
	SearchTask struct {
		ServerName       string
		WebServerAddr    string
		Login            string
		Password         string
		CamDisplayID     int
		CamNameForReport string
		Rule             RuleDefinition
	}

	RuleDefinition struct {
		QueryType  string    `json:"queryType"`
		Conditions Condition `json:"conditions"`
		Figures    []Figure  `json:"figures"`
	}

	Condition struct {
		Duration int `json:"duration"`
	}

	Figure struct {
		Shape []Point `json:"shape"`
	}

	// search result models
	SearchResult struct {
		CamNameForReport string `json:"-"`
		Intervals        []Interval
	}

	Interval struct {
		StartTime string `json:"startTime"`
		EndTime   string `json:"endTime"`
	}

	//models for get camera and detector info
	ForGetCamerasInfo struct {
		Cameras []struct {
			DisplayID   string `json:"displayId"`
			DisplayName string `json:"displayName"`
		} `json:"cameras"`
	}

	ForGetDetector struct {
		Detectors []struct {
			Name string `json:"name"`
			Type string `json:"type"`
		} `json:"detectors"`
	}
)
