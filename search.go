package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/go-errors/errors"

	"go.uber.org/zap"
)

const workersNum = 3

type Searcher struct {
	startTime        string
	endTime          string
	logger           *zap.Logger
	httpClient       *http.Client
	searchTaskChan   chan *SearchTask
	searchResultChan chan *SearchResult
	wgSearchExec     *sync.WaitGroup
	wgSearchResults  *sync.WaitGroup
	timeLocation     *time.Location
}

func newSearcher(startTime, endTime string, logger *zap.Logger) (*Searcher, error) {
	localTimeLocation, err := time.LoadLocation("Local")
	if err != nil {
		return nil, err
	}
	return &Searcher{
		startTime:        startTime,
		endTime:          endTime,
		logger:           logger,
		httpClient:       &http.Client{Timeout: time.Minute * 5},
		searchTaskChan:   make(chan *SearchTask),
		searchResultChan: make(chan *SearchResult),
		wgSearchExec:     &sync.WaitGroup{},
		wgSearchResults:  &sync.WaitGroup{},
		timeLocation:     localTimeLocation,
	}, nil
}

func (s *Searcher) Search(settings *Settings, reportPath string) {
	s.logger.Info("Start search")
	csvFile, err := os.Create(reportPath)
	if err != nil {
		panic(fmt.Sprintf("fail to create report file: %s", err.Error()))
	}
	defer csvFile.Close()

	s.startSearchResultsServer(csvFile)

	s.startSearchWorkers()
	searchTasks := convertSettingToSearchTasks(settings)
	for _, task := range searchTasks {
		s.searchTaskChan <- task
	}
	close(s.searchTaskChan)
	s.wgSearchExec.Wait()
	close(s.searchResultChan)
	s.wgSearchResults.Wait()
	s.logger.Info(fmt.Sprintf("Search finished report saved to path: %s", reportPath))
}

func (s *Searcher) searchWorker() {
	defer s.wgSearchExec.Done()
	for task := range s.searchTaskChan {
		if err := s.execSearchByTask(task); err != nil {
			s.logger.Error("search execute fail",
				zap.String("server", task.ServerName),
				zap.Int("camera displayId", task.CamDisplayID),
				zap.String("err", err.Error()),
			)
		}
		runtime.Gosched()
	}
}

func (s *Searcher) startSearchWorkers() {
	for i := 0; i < workersNum; i++ {
		s.wgSearchExec.Add(1)
		go s.searchWorker()
	}
}

func (s *Searcher) execSearchByTask(task *SearchTask) error {
	s.logger.Info("execSearchByTask",
		zap.String("server", task.ServerName),
		zap.Int("camera displayId", task.CamDisplayID),
	)
	detectorID, err := s.getDetectorID(task)
	if err != nil {
		return errors.WrapPrefix(err, "getDetectorID fail", 1)
	}
	searchID, err := s.execSearchToAN(task, detectorID)
	if err != nil {
		return errors.WrapPrefix(err, "execSearchToAN fail", 1)
	}
	searchResult, err := s.getSearchResult(searchID, task)
	if err != nil {
		return errors.WrapPrefix(err, "getSearchResult fail", 1)
	}
	s.logger.Info("got search result",
		zap.Int("Len", len(searchResult.Intervals)),
		zap.String("Camera", searchResult.CamNameForReport),
	)
	s.searchResultChan <- searchResult
	return nil
}

func (s *Searcher) execSearchToAN(task *SearchTask, detectorID string) (string, error) {
	s.logger.Info("execSearchToAN",
		zap.String("server", task.ServerName),
		zap.Int("camera displayId", task.CamDisplayID),
	)

	dID := convertDetectorIDToSearchID(detectorID)
	body, err := json.Marshal(task.Rule)
	if err != nil {
		return "", errors.WrapPrefix(err, "search request body encode error", 1)
	}

	url := fmt.Sprintf("http://%s/search/vmda/%s/SourceEndpoint.vmda/%s/%s", task.WebServerAddr, dID, s.startTime, s.endTime)
	resp, err := s.execRequest(http.MethodPost, url, bytes.NewReader(body), task)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return "", err
	}
	searchID := resp.Header.Get("Location")
	if searchID == "" {
		return "", errors.New("server not received search id ")
	}
	return searchID, nil
}

func (s *Searcher) getSearchResult(searchID string, task *SearchTask) (*SearchResult, error) {
	s.logger.Info("getSearchResult",
		zap.String("server", task.ServerName),
		zap.Int("camera displayId", task.CamDisplayID),
	)
	defer func() {
		err := s.deleteSearch(task, searchID)
		if err != nil {
			s.logger.Error("search delete fail",
				zap.String("server", task.ServerName),
				zap.Int("camera displayId", task.CamDisplayID),
				zap.String("searchID", searchID),
				zap.String("err", err.Error()),
			)
		}
	}()

	url := fmt.Sprintf("http://%s%s/result", task.WebServerAddr, searchID)
	var err error
	var resp *http.Response
	for {
		resp, err = s.execRequest(http.MethodGet, url, nil, task)
		if resp != nil {
			defer resp.Body.Close()
		}
		if err != nil {
			return nil, err
		}

		switch resp.StatusCode {
		case http.StatusOK:
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, errors.WrapPrefix(err, "read body fail", 1)
			}
			result := &SearchResult{}
			err = json.Unmarshal(data, &result)
			if err != nil {
				return nil, errors.WrapPrefix(err, "cant unpack result json", 1)
			}
			result.CamNameForReport = task.CamNameForReport
			return result, nil
		case http.StatusPartialContent:
			s.logger.Info("got partial content response")
			resp.Body.Close()
			time.Sleep(1 * time.Second)
			continue
		case http.StatusNotFound:
			return nil, errors.New("search GUID not found")
		default:
			return nil, errors.Errorf("unknown error with response status code %d", resp.StatusCode)
		}
	}
}

func (s *Searcher) deleteSearch(task *SearchTask, searchID string) error {
	s.logger.Info("deleteSearch",
		zap.String("server", task.ServerName),
		zap.String("searchID", searchID),
	)

	url := fmt.Sprintf("http://%s%s", task.WebServerAddr, searchID)
	resp, err := s.execRequest(http.MethodDelete, url, nil, task)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	return nil
}

func (s *Searcher) getDetectorID(task *SearchTask) (string, error) {
	s.logger.Info("getDetectorForSearch",
		zap.String("server", task.ServerName),
		zap.Int("camera displayId", task.CamDisplayID),
	)
	url := fmt.Sprintf("http://%s/detectors/%s/DeviceIpint.%d", task.WebServerAddr, task.ServerName, task.CamDisplayID)
	resp, err := s.execRequest(http.MethodGet, url, nil, task)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return "", err
	}
	if resp.StatusCode == http.StatusForbidden {
		return "", errors.New("cam not found")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.WrapPrefix(err, "read body fail", 1)
	}
	data := &ForGetDetector{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return "", errors.WrapPrefix(err, "cant unpack result json", 1)
	}
	for _, detector := range data.Detectors {
		if detector.Type == "SceneDescription" || detector.Type == "NeuroTracker" {
			return detector.Name, nil
		}
	}
	return "", errors.New("scene description detector for cam not found")
}

func (s *Searcher) execRequest(method, url string, body io.Reader, task *SearchTask) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	if method == http.MethodPost {
		req.Header.Set("Content-Type", "application/json")
	}
	req.SetBasicAuth(task.Login, task.Password)
	resp, err := s.httpClient.Do(req)
	if err != nil {
		if err, ok := err.(net.Error); ok && err.Timeout() {
			return resp, errors.Errorf("timeout error %s", err.Error())
		}
		return resp, errors.Errorf("unknown error %s", err.Error())
	}

	switch resp.StatusCode {
	case http.StatusUnauthorized:
		return resp, errors.New("unauthorized")
	case http.StatusInternalServerError:
		return resp, errors.New("server fatal error")
	case http.StatusBadRequest:
		return resp, errors.New("bad request error")
	}
	return resp, nil
}

func (s *Searcher) startSearchResultsServer(csvFile io.Writer) {
	s.wgSearchResults.Add(1)
	go s.writeSearchResultsToCSV(csvFile)
}

func (s *Searcher) writeSearchResultsToCSV(csvFile io.Writer) {
	s.logger.Info("searchResultToCSV start")
	var err error
	w := csv.NewWriter(csvFile)
	defer func() {
		w.Flush()
		s.wgSearchResults.Done()
	}()
	title := []string{"ServerName_CameraName_DisplayID", "EntryTime", "OutputTime"}
	if err = w.Write(title); err != nil {
		s.logger.Error("fail write row to csv file")
	}
	for result := range s.searchResultChan {
		for _, interval := range result.Intervals {
			if err = w.Write([]string{
				result.CamNameForReport,
				s.anTimeToReportFormat(interval.StartTime),
				s.anTimeToReportFormat(interval.EndTime),
			}); err != nil {
				s.logger.Error(fmt.Sprintf("fail write row to csv file: %s", err.Error()))
			}
		}
	}
}

func (s *Searcher) anTimeToReportFormat(anTime string) string {
	tm, _ := time.Parse(timeFormat, anTime)
	tm = tm.In(s.timeLocation)
	return fmt.Sprintf("%02d/%02d/%04d %02d:%02d:%02d", tm.Day(), tm.Month(), tm.Year(), tm.Hour(), tm.Minute(), tm.Second())
}

func convertSettingToSearchTasks(settings *Settings) []*SearchTask {
	tasks := make([]*SearchTask, 0, 16)
	for _, entity := range settings.Settings {
		for _, cam := range entity.Cameras {
			tasks = append(tasks, &SearchTask{
				ServerName:       entity.ServerName,
				WebServerAddr:    entity.WebServerAddr,
				Login:            entity.Login,
				Password:         entity.Password,
				CamDisplayID:     cam.ID,
				CamNameForReport: cam.Name,
				Rule: RuleDefinition{
					QueryType: "zone",
					Conditions: Condition{
						Duration: cam.Duration,
					},
					Figures: []Figure{
						{
							Shape: cam.Shape,
						},
					},
				},
			})
		}
	}
	return tasks
}

func convertDetectorIDToSearchID(id string) string {
	t := strings.TrimPrefix(id, "hosts/")
	return strings.TrimSuffix(t, "/EventSupplier")
}
